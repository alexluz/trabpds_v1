<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <title><?php 
  			$title = "SGE - Sistema de Gerenciamento Escolar";
  			echo $title; ?></title>
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Bootstrap theme -->
  <link href="css/bootstrap-theme.min.css" rel="stylesheet">
  
  <!-- Custom styles for this template -->
  <link href="css/theme.css" rel="stylesheet">
  <link href="css/jquery-ui.css" rel="stylesheet">
  
  <!-- JQUERY Library -->
  <script src="js/jquery.min.js"></script>
</head>
<body>
	<div class='container' id='main-container'>
	<div class="clearfix row">
	  <div class='col-md-12' id='logo'><p id='logo_bar'>SGE</p>
	  </div>
	</div>


	<div class="loginform-in col-md-3" id="login-div">
	  <h3 style='padding-left:30px;'>Login</h3>
	  <div class="err" id="add_err_login"></div>
	  <fieldset>
		<form action="./" method="post">
		  <table>
			<tr>
		      <td> <label for="name">Usuario</label></td>
			  <td><input type="text" size="20em" name="user" id="username"/></td>
			</tr>
			<tr>
		      <td> <label for="name">Senha</label></td>
		      <td><input type="password" size="20em"  name="pw" id="senha"/></td>
			</tr>
		  </table>
		  <input type="submit" id="login" name="login" value="Login" class="loginbutton" onclick='logIn(this)' >
        </form>
	  </fieldset>
	</div>
	
</div>
</body>

<!-- Custom JS Functions for this template -->
<script src="js/theme.js"></script>
</html>