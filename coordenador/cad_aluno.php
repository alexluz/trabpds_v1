<?php 

?>

<!-- Título do butão pressionado -->
        <div class="navbar-text col-lg-12" style="text-align: center">
            <h1> Cadastro de Alunos</h1>
        </div>    
<!-- Fim do Título do botão pressionado -->
        
        <div class="col-xs-12" style="text-align: center">
            
            <form class="form-horizontal">
        <div class="form-group">
            <label for="inputNome" class="control-label col-xs-2">Nome</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputNomeAluno" placeholder="Primeiro Nome">
            </div>
        </div>
        <div class="form-group">
            <label for="inputSnome" class="control-label col-xs-2">Sobrenome</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputSnomeAluno" placeholder="Sobrenome">
            </div>
        </div>
        <div class="form-group">
            <label for="inputDataNasc" class="control-label col-xs-2">Data de Nascimento</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputDatanascAluno" placeholder="dd/mm/aaaa">
            </div>
        </div>
        <div class="form-group">
            <label for="inputSexo" class="control-label col-xs-2">Sexo</label>
            <div class="col-xs-2">
                <input type="radio" name="sex" value="F">Feminino
            </div>
            <div class="col-xs-2">
                <input type="radio" name="sex" value="M">Masculino
            </div>
        </div>
        <div class="form-group">
            <label for="inputCidade" class="control-label col-xs-2">Cidade</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputCidadeAluno" placeholder="Cidade">
            </div>
        </div>
        <div class="form-group">
            <label for="inputRua" class="control-label col-xs-2">Rua e Nº</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputRuaAluno" placeholder="Rua e Número">
            </div>
        </div>
        <div class="form-group">
            <label for="inputCep" class="control-label col-xs-2">Cep</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputCepAluno" placeholder="CEP">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">E-Mail</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputEmailAluno" placeholder="email@exemplo.com">
            </div>
        </div>
        <div class="form-group">
            <label for="inputLogin" class="control-label col-xs-2">Login</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputLoginAluno" placeholder="login">
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword" class="control-label col-xs-2">Senha</label>
            <div class="col-xs-10">
                <input type="password" class="form-control" id="inputPasswordAluno">
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-1">
                <button type="button" class="btn btn-primary" onclick="cad_aluno()">Cadastrar</button>
            </div>
            <div class="col-xs-offset-2 col-xs-3">
                <button type="button" class="btn btn-danger">Cancelar</button>
            </div>
        </div>
    </form>
</div>