<?php 

?>

<!-- Título do butão precionado -->
        <div class="navbar-text col-md-12" style="text-align: center">
            <h1> Cadastro de Disciplinas</h1>
        </div>    
<!-- Fim do Título do butão precionado -->
        
        
<!-- Página de Cadastro -->
        <div class="col-lg-12" style="text-align: center">
            <form class="form-horizontal">
                <div class="form-group">
                    <label for="inputNome" class="control-label col-xs-2">Nome</label>
                    <div class="col-xs-10">
                        <input type="text" class="form-control" id="inputNome" placeholder="Nome da disciplina">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputNome" class="control-label col-xs-2">Carga Horária</label>
                    <div class="col-xs-10">
                        <input type="text" class="form-control" id="inputCarga" placeholder="Carga horaria">
                    </div>
                </div>                   
                <div class="form-group">
                    <div class="col-xs-offset-2 col-xs-1">
                        <button type="button" class="btn btn-primary" onclick="cad_disc()">Cadastrar</button>
                    </div>
                    <div class="col-xs-offset-2 col-xs-3">
                        <button type="button" class="btn btn-danger">Cancelar</button>
                    </div>
                </div>
            </form>
        </div>
<!-- Fim da Página de Cadastro -->