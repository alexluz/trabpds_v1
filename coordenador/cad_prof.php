<!-- TÃ­tulo do butÃ£o precionado -->
        <div class="navbar-text col-lg-12" style="text-align: center">
            <h1> Cadastro de Professores</h1>
        </div>    
<!-- Fim do TÃ­tulo do butÃ£o precionado -->
        
        <div class="col-xs-12" style="text-align: center">
            
            <form class="form-horizontal">
        <div class="form-group">
            <label for="inputNome" class="control-label col-xs-2">Nome</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputNomeProf" placeholder="Primeiro Nome">
            </div>
        </div>
        <div class="form-group">
            <label for="inputSnome" class="control-label col-xs-2">Sobrenome</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputSnomeProf" placeholder="Sobrenome">
            </div>
        </div>
        <div class="form-group">
            <label for="inputCep" class="control-label col-xs-2">CPF</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputCPFProf" placeholder="CPF">
            </div>
        </div>
        <div class="form-group">
            <label for="inputCep" class="control-label col-xs-2">RG</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputRGProf" placeholder="RG">
            </div>
        </div>
        
        
        <div class="form-group">
            <label for="inputCidade" class="control-label col-xs-2">Cidade</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputCidadeProf" placeholder="Cidade">
            </div>
        </div>
        <div class="form-group">
            <label for="inputRua" class="control-label col-xs-2">Rua e Nº</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputRuaProf" placeholder="Rua e Nº">
            </div>
        </div>
        <div class="form-group">
            <label for="inputCep" class="control-label col-xs-2">Cep</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputCepProf" placeholder="CEP">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">E-Mail</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputEmailProf" placeholder="email@exemplo.com">
            </div>
        </div>
        <div class="form-group">
            <label for="inputCep" class="control-label col-xs-2">Telefone</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputTelefoneProf" placeholder="Telefone">
            </div>
        </div>
        <div class="form-group">
            <label for="inputLogin" class="control-label col-xs-2">Login</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputLoginProf" placeholder="login">
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword" class="control-label col-xs-2">Senha</label>
            <div class="col-xs-10">
                <input type="password" class="form-control" id="inputPasswordProf">
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-1">
                <button type="button" id="cad_bt_p" class="btn btn-primary" onclick="cad_professor()">Cadastrar</button>
            </div>
            <div class="col-xs-offset-2 col-xs-3">
                <button type="button" class="btn btn-danger">Cancelar</button>
            </div>
        </div>
    </form>
</div>