<?php $title = "SGE - Sistema de Gerenciamento Escolar"; 
      include_once("header.php"); ?>

    <div class="clearfix row">
        <div class="col-md-3" style="background-color:lightcyan; height:600px;" id="sidebar">

    <!-- Butão de início -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title"><b> Home </b></h2>
                    </div>
                    <div class="panel-body">
                        <form>
                            <input class="btn btn-default col-md-12" type="button" value="Início">
                            <br> <br>
                        </form>
                    </div>
                </div>
    <!-- Fim do Butões de início -->


    <!-- Butões do lado esquerdo da tela de Cadastro -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title"><b> Cadastrar </b></h2>
                    </div>
                    <div class="panel-body">
                        <form>
                            <input class="btn btn-default col-md-12" type="button" value="Professor" onclick="javascript: location.href='cadastro_prof.php';">
                            <br> <br>

                            <input class="btn btn-default col-md-12" type="button" value="Aluno" onclick="javascript: location.href='cadastro_aluno.php';">
                            <br> <br>
                            <input class="btn btn-default col-md-12" type="button" value="Disciplina" onclick="javascript: location.href='cadastro_disciplina.php';">

                        </form>
                    </div>
                </div>
    <!-- Fim dos Butões do lado esquerdo da tela de Cadastro -->

    <!-- Butão de Notíciais -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title"><b> Notíciais </b></h2>
                    </div>
                    <div class="panel-body">
                        <form>
                            <input class="btn btn-default col-md-12" type="button" value="Escrever">
                            <br> <br>
                        </form>
                    </div>
                </div>
    <!-- Fim do Butões de Notíciais -->

    <!-- Botão de sair -->
                <button type="button" class="btn btn-default btn-lg" onclick="javascript: location.href='index.php';">
                  <span class="glyphicon glyphicon-off" aria-hidden="true"></span> Sair
                </button>
    <!-- Fim do Botão de sair -->

        </div>
        <div class="col-md-9" style="background-color:lightseagreen; height:600px;"></div>

    </div>
<?php include_once("footer.php"); ?>