<?php 
require_once("../php/dbconnection.php");
?>


<div class="navbar-text col-lg-12" style="text-align: center">
  <h1> Cadastro de Turmas</h1>
</div> 

<div class="col-xs-12" style="text-align: center">
    <form class="form-horizontal">
	    <!--SELECIONAR DISCIPLINA-->
	    <div class="form-group col-xs-4">
		  <label for="disc_selected">Disciplina:</label>
		  <select class="form-control" id="disc_selected">
		    <?php 
		  		$query = "SELECT id, nome FROM disciplinas";
				$results = @mysqli_query($dbc, $query);
				while($disciplina = mysqli_fetch_array($results)){
					echo '<option id="'.$disciplina["id"].'">'.$disciplina["nome"].'</option>';
				}
			?>
		    </select>
		</div>
		<!--SELECIONAR PROFESSOR-->
		<div class="form-group col-xs-4 col-md-offset-3">
		  <label for="prof_selected">Professor:</label>
		  <select class="form-control" id="prof_selected">
		    <?php 
		  		$query = "SELECT id, nome, s_nome FROM professores";
				$results = @mysqli_query($dbc, $query);
				while($professor = mysqli_fetch_array($results)){
					echo '<option id="'.$professor["id"].'">'.$professor["nome"].' '.$professor["s_nome"].'</option>';
				}
			?>
		    </select>
		</div>		
		<!--SELECIONAR ALUNOS-->
		<div class="form-group col-xs-5">
		  <label for="sel1">Alunos:</label>
		  <div class="dropdown">
			  <div class="btn btn-default dropdown-toggle col-xs-5" type="button" data-toggle="dropdown">Alunos
			  <span class="caret"></span></div>
			  <ul class="dropdown-menu dropdown-alunos">
			    <li>
			    	<div class="scrollable_checkbox">
			    	  <?php 
				  		$query = "SELECT id, nome, s_nome FROM alunos";
						$results = @mysqli_query($dbc, $query);
						while($aluno = mysqli_fetch_array($results)){
							echo '<label class="checkbox">
								  <input type="checkbox" name="alunos_selected" id="'.$aluno["id"].'">'.$aluno["nome"].' '.$aluno["s_nome"].'</label>';
						}
					  ?>
			    	</div>
			    </li>
			  </ul>
		  </div>
		</div>
		<!--SELECIONAR Horario-->
		<div class="form-group col-xs-5">
			<label for="inputHor" class="control-label col-xs-2">Horario</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputHor" placeholder="HH:MM">
            </div>
		</div>
		<!--SELECIONAR Local Aula-->
		<div class="form-group col-xs-5">
			<label for="inputLocal" class="control-label col-xs-2">Local</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputLocal" placeholder="Local da Aula">
            </div>
		</div>
	    <div class="form-group">
          <div class="col-xs-offset-2 col-xs-1">
            <button type="button" id="cad_bt_p" class="btn btn-primary" onclick="cad_turma()">Cadastrar</button>
          </div>
          <div class="col-xs-offset-2 col-xs-3">
            <button type="button" class="btn btn-danger">Cancelar</button>
          </div>
        </div>
    </form>
</div>