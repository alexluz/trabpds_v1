<?php
require_once("../php/dbconnection.php");
if(session_id()==''||!isset($_SESSION)){
  session_start();
}

$id = $_SESSION['id_turma'];
$query = "SELECT data_av1, data_av2, data_av3 FROM avaliacoes WHERE id_turma=$id";
$results = mysqli_query($dbc,$query);
$row = mysqli_fetch_array($results);
?>

<div class="navbar-text col-lg-12" style="text-align: center">
  <h1> <?php echo $_SESSION['nome_turma']; ?> </h1>
</div>

<table class="table">
	<thead>
		<th>Tipo</th>
		<th>Data</th>
	</thead>
	<tbody>
		<tr>
  			<td>Avaliação 1</td>
  			<td><input type="date" <?php echo 'value="'.$row["data_av1"].'"'; ?> id="av1">
  				<div class="btn btn-success col-md-offset-1" onclick="addAv(1)">Confirmar data</div>
  				</input></td>
  		  </tr>
  		  <tr>
  			<td>Avaliação 2</td>
  			<td><input type="date"  <?php echo 'value="'.$row["data_av2"].'"'; ?> id="av2">
  				<div class="btn btn-success col-md-offset-1" onclick="addAv(2)">Confirmar data</div>
  				</input></td>
  		  </tr>
  		  <tr>
  			<td>Avaliação 3</td>
  			<td><input type="date"  <?php echo 'value="'.$row["data_av3"].'"'; ?> id="av3">
  				<div class="btn btn-success col-md-offset-1" onclick="addAv(3)">Confirmar data</div>
  				</input></td>
  		  </tr>
	</tbody>
</table>