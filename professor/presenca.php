<?php
require_once("../php/dbconnection.php");
if(session_id()==''||!isset($_SESSION)){
	session_start();
}
$id = $_SESSION["id_turma"];
$query = "SELECT alunos.id, nome, s_nome FROM alunos,turmas_alunos WHERE alunos.id = turmas_alunos.id_aluno AND turmas_alunos.id_turma = $id";
$results = mysqli_query($dbc,$query);
?>
<div class="navbar-text col-lg-12" style="text-align: center">
  <h1> <?php echo $_SESSION['nome_turma']; ?> </h1>
</div>

<table class="table">
<thead>
	<th>Nome</th>
	<th>Presente</th>
</thead>
<tbody>
<?php 
	while($row = mysqli_fetch_array($results)){
		echo '
			<tr>
				<td>'.$row["nome"].' '.$row["s_nome"].'</td>
				<td>
					<div class="btn btn-success" id="'.$row["id"].'" onclick="addPresenca(this,1)">Sim</div>
					<div class="btn btn-danger" id="'.$row["id"].'" onclick="addPresenca(this,0)">Não</div>
				</td>
			</tr>';
	}
?>
</tbody>
</table>