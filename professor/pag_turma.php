<?php
if(session_id()==''||!isset($_SESSION)){
	session_start();
}
require_once("../php/dbconnection.php");

if(isset($_GET['id'])){
	$_SESSION['id_turma'] = $_GET['id'];
	$_SESSION['nome_turma'] = $_GET['nome'];
}

?>

<div class="navbar-text col-lg-12" style="text-align: center">
  <h1> <?php echo $_SESSION['nome_turma']; ?> </h1>
</div>

<div class="row">
  <div class="col-xs-4 btn btn-success" onclick="gotoPresenca()">Add Presenca</div>
  <div class="col-xs-4 btn btn-success" onclick="gotoNotas(1)">Add Nota</div>
  <div class="col-xs-4 btn btn-success" onclick="gotoAvaliacao(1)">Add Avaliacao</div>
  <div class="col-xs-4 btn btn-success" onclick="gotoNoticia(1)">Add Noticia</div>
  <div class="col-xs-4 btn btn-primary" onclick="gotoDownUpFiles(1)">Download/Upload Ativ.</div>
  <div class="col-xs-4 btn btn-danger" onclick="fecharTurma(<?php echo $_SESSION['id_turma']; ?>)">Fechar Turma</div>
</div>