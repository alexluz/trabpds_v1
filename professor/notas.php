<?php
require_once("../php/dbconnection.php");
if(session_id()==''||!isset($_SESSION)){
  session_start();
}
$id = $_SESSION['id_turma'];
//retorna id, nome, sobrenome, nota1, nota2, nota3, rec de todos os alunos da turma atual
$query = "SELECT a.id, nome, s_nome, nota1, nota2, nota3, rec FROM alunos as a,turmas_alunos as t,notas as n WHERE a.id = t.id_aluno AND t.id_turma=$id AND n.id_turma=t.id_turma AND n.id_aluno=a.id";
$results = mysqli_query($dbc,$query);
?>
<!-- Título do butão precionado -->
        <div class="navbar-text col-lg-12" style="text-align: center">
          <h1> <?php echo $_SESSION['nome_turma']; ?> </h1>
        </div>
        <div>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Nome</th>
                <th>Unidade 1</th>
                <th>Unidade 2</th>
                <th>Unidade 3</th>
                <th>Recuperação</th>
                <th>Alterar Notas</th>
              </tr>
              <tbody>
                <?php
                  while($row = mysqli_fetch_array($results)){
                    echo '<tr>
                            <td>'.$row["nome"].' '.$row["s_nome"].'</td>
                            <td><input class="aluno'.$row['id'].' n1" type="number" placeholder="'.$row["nota1"].'"/></td>
                            <td><input class="aluno'.$row['id'].' n2" type="number" placeholder="'.$row["nota2"].'"/></td>
                            <td><input class="aluno'.$row['id'].' n3" type="number" placeholder="'.$row["nota3"].'"/></td>
                            <td><input class="aluno'.$row['id'].' rec" type="number" placeholder="'.$row["rec"].'"/></td>
                            <td><div class="btn btn-success" onclick="addNotas('.$row['id'].')">Confirmar</div></td>
                          </tr>';
                  }
                ?>
              </tbody>  
            </thead>
      </table>
        </div>