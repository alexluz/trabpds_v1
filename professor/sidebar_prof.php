<?php
require_once("../php/dbconnection.php");
if(session_id()==''||!isset($_SESSION)){
  session_start();
}
$id_prof = $_SESSION['id_prof'];
//$id_prof = 3;
$query = "SELECT d.nome, t.id FROM disciplinas as d, turmas as t, professores as p WHERE p.id=".$id_prof." AND t.id_prof=p.id AND t.id_disc=d.id";
$results = mysqli_query($dbc, $query);
?>
<!-- Butão de início -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title"><b> Home </b></h2>
                </div>
                <div class="panel-body">
                    <form>
                        <input class="btn btn-default col-md-12" type="button" value="Início">
                        <br> <br>
                    </form>
                </div>
            </div>
<!-- Fim do Butões de início -->
        
        
<!-- Butões do lado esquerdo da tela de Cadastro -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title"><b> Turmas </b></h2>
            </div>
            <div class="panel-body">


                <form>
                    <?php
                      while($row = mysqli_fetch_array($results)){
                        echo '<input class="btn btn-default col-md-12" type="button" 
                                value="'.$row['nome'].'" id="'.$row['id'].'" onclick="gotoDisc(this,1)">
                            <br> <br>';
                      }
                    ?>
                </form>
            </div>
        </div>
    
<!-- Fim dos Butões do lado esquerdo da tela de Cadastro -->
        
<!-- Botão de sair -->
            <a href="../index.php"><button type="button" class="btn btn-default btn-lg" id="logout_bt">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span> Sair
            </button></a>
<!-- Fim do Botão de sair -->