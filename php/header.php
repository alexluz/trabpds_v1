<?php 
//require_once("dbconnection.php");
//$db_session = new DB(); 
?>
<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <title><?php echo $title; ?></title>
  <!-- JQUERY Library -->
  <!-- se esses js não forem chamados primeiro, os botões dropdown não vão funcionar-->
  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <!-- Bootstrap core CSS -->
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <!-- Bootstrap theme -->
  <link href="../css/bootstrap-theme.min.css" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="../css/theme.css" rel="stylesheet">
  <link href="../css/jquery-ui.css" rel="stylesheet">
</head>
<body>
<div class='container' id='main-container'>
<div class="clearfix row">
  <div class='col-md-12' id='logo'><p id='logo_bar'>SGE</p>
  </div>
</div>
