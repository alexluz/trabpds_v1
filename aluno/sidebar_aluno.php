
<!-- Butão de início -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title"><b> Home </b></h2>
                </div>
                <div class="panel-body">
                    <form>
                        <input class="btn btn-default col-md-12" type="button" value="Início">
                        <br> <br>
                    </form>
                </div>
            </div>
<!-- Fim do Butões de início -->
        
        
<!-- Butões do lado esquerdo da tela de Cadastro -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title"><b> Disciplinas </b></h2>
            </div>
            <div class="panel-body">


                <form>
                    <?php
                        require_once("../php/dbconnection.php");
                        $id_aluno = $_SESSION['id_aluno'];
                        //$id_aluno = 1;
                        $query = "SELECT d.nome, t.id FROM turmas as t, disciplinas as d, alunos as a, turmas_alunos as ta WHERE a.id=$id_aluno AND a.id=ta.id_aluno AND ta.id_turma=t.id AND t.id_disc=d.id";
                        $results = mysqli_query($dbc, $query);
                        while($row = mysqli_fetch_array($results)){
                            echo '<input class="btn btn-default col-md-12" type="button" value="'.$row['nome'].'" id="'.$row['id'].'" onclick="gotoDisc(this,2)"><br><br>';
                        }
                    ?>     
                </form>
            </div>
        </div>
    
<!-- Fim dos Butões do lado esquerdo da tela de Cadastro -->
    
<!-- Butão de Notíciais -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title"><b> Historico </b></h2>
                </div>
                <div class="panel-body">
                    <form>
                        <input class="btn btn-default col-md-12" type="button" value="Ver Historico" onclick="verHistorico()">
                        <br> <br>
                    </form>
                </div>  
            </div>
<!-- Fim do Butões de Notíciais --> 
        
<!-- Botão de sair -->
            <a href="../index.php"><button type="button" class="btn btn-default btn-lg" id="logout_bt">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span> Sair
            </button></a>
<!-- Fim do Botão de sair -->