<?php
require_once("../php/dbconnection.php");
if(session_id()==''||!isset($_SESSION)){
  session_start();
}
$id = $_SESSION['id_turma'];
$query = "SELECT data_av1, data_av2, data_av3 FROM avaliacoes WHERE id_turma=$id";
$results = mysqli_query($dbc,$query);
?>

<div class="navbar-text col-lg-12" style="text-align: center">
  <h1> <?php echo $_SESSION['nome_turma']; ?> </h1>
</div>

<table class="table">
	<thead>
		<th>Tipo</th>
		<th>Data</th>
	</thead>
	<tbody>
		<?php 
		  while($row = mysqli_fetch_array($results)){
		  	echo '<tr>
		  			<td>Avaliação 1</td>
		  			<td>'.$row["data_av1"].'</td>
		  		  </tr>
		  		  <tr>
		  			<td>Avaliação 2</td>
		  			<td>'.$row["data_av2"].'</td>
		  		  </tr>
		  		  <tr>
		  			<td>Avaliação 3</td>
		  			<td>'.$row["data_av3"].'</td>
		  		  </tr>';
		  }
		?>	
	</tbody>
</table>