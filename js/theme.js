function logIn(element) {
	$('#login-div').css('display','none');

	
	var user = $('#username').val();
	var pw = $('#senha').val();
	$.ajax({
			type: 'POST',//type: POST/GET
			url: 'login.php',//page to where the data goes
			data: { "user" : user,
					"senha" : pw,
					"logar" : 1},//type of data to be sent
			cache: false,//don't save in cache
			//"result" variable stores the data sent from server to client
			success: 	function(result){//if successful run from here
                            console.log(result);
       //                      if(result==0 || result=='0'){
       //                          window.location.href="/trabpds/coordenador/coordenador.php";
       //                      }
       //                      else if(result==1 || result=='1'){
       //                          window.location.href="/trabpds/professor/professor.php";
       //                      }
							// else{
       //                          window.location.href="http://google.com";
       //                      }
						},
			error: 		function(result){//if not successful run from here

						}
	});
}

$(document).ready(function(){

  $("#cad_prof").click(function(){
    $("#coord_main").load("cad_prof.php");
  });

  $("#cad_aluno").click(function(){
    $("#coord_main").load("cad_aluno.php");
  });

  $("#cad_disc").click(function(){
    $("#coord_main").load("cad_disc.php");
  });

  $("#cad_turma").click(function(){
    $("#coord_main").load("cad_turma.php");
  });

});

function gotoPresenca(){
    $("#prof_main").load("presenca.php");
}

function gotoNotas(index){
    if(index==1)
        $("#prof_main").load("notas.php");
    else
        $("#aluno_main").load("notas.php");
}

function gotoAvaliacao(index){
    if(index==1)
        $("#prof_main").load("avaliacao.php");
    else
        $("#aluno_main").load("avaliacao.php");
}

function gotoNoticia(index){
    if(index==1)
        $("#prof_main").load("noticia.php");
    else
        $("#aluno_main").load("noticia.php");
}

function gotoDownUpFiles(index){
    if(index==1)
        $("#prof_main").load("up_down_arquivos.php");
    else
        $("#aluno_main").load("up_down_arquivos.php"); 
}

function fecharTurma(element){
    var id_turma = element;
}

function verHistorico(){
    $("#aluno_main").load("historico.php");
}

function addAv(element){
    if(element == 1)
        var av_data = $("#av1").val();
    else if(element == 2)
        var av_data = $("#av2").val();
    else
        var av_data = $("#av3").val();
    $.ajax({
        type: 'POST',//type: POST/GET
        url: '../php/config.php',//page to where the data goes
        data: { 
                "avaliacao":element,
                "av_data" : av_data
              },//type of data to be sent
        cache: false,//don't save in cache
        //"result" variable stores the data sent from server to client
        success:    function(result){//if successful run from here
                        console.log(result);
                        gotoAvaliacao(1);
                    },
        error:      function(result){//if not successful run from here
                        }
    });
}

function addNotas(id_aluno){
    var classe = ".aluno"+id_aluno;
    //alert(id_aluno);
    var notas = new Array();
    $.each($(classe),function(){
        notas.push($(this).val());
    });
    var n1 = notas[0];
    var n2 = notas[1];
    var n3 = notas[2];
    var rec = notas[3];

    $.ajax({
        type: 'POST',//type: POST/GET
        url: '../php/config.php',//page to where the data goes
        data: { 
                "id_aluno":id_aluno,
                "nota1" : n1,
                "nota2" : n2,
                "nota3" : n3,
                "rec" : rec,
                "addNota" : 1
              },//type of data to be sent
        cache: false,//don't save in cache
        //"result" variable stores the data sent from server to client
        success:    function(result){//if successful run from here
                        console.log(result);
                        $("#prof_main").load("notas.php"); 
                    },
        error:      function(result){//if not successful run from here
                        }
    });

}

function addPresenca(element,index){
    var id_aluno = element.id;

    $.ajax({
        type: 'POST',//type: POST/GET
        url: '../php/config.php',//page to where the data goes
        data: { 
                "addPresenca":1,
                "id_aluno" : id_aluno,
                "presente" : index
              },//type of data to be sent
        cache: false,//don't save in cache
        //"result" variable stores the data sent from server to client
        success:    function(result){//if successful run from here
                        console.log(result);
                        $("#prof_main").load("presenca.php"); 
                    },
        error:      function(result){//if not successful run from here
                        }
    });
}

function addNoticia(){
    var element = $("#addnoticia").val();
    $.ajax({
        type: 'POST',//type: POST/GET
        url: '../php/config.php',//page to where the data goes
        data: { 
                "noticia":element
              },//type of data to be sent
        cache: false,//don't save in cache
        //"result" variable stores the data sent from server to client
        success:    function(result){//if successful run from here
                        $("#prof_main").load("noticia.php"); 
                    },
        error:      function(result){//if not successful run from here
                        }
    });
}

function gotoDisc(element,index){
    var id = element.id;
    var nome = element.value;
    $.ajax({
        type: 'GET',//type: POST/GET
        url: 'pag_turma.php',//page to where the data goes
        data: { 
                "id":id,
                "nome" : nome
              },//type of data to be sent
        cache: false,//don't save in cache
        //"result" variable stores the data sent from server to client
        success:    function(result){//if successful run from here
                        if(index==1)
                            $("#prof_main").load("pag_turma.php");
                        else
                            $("#aluno_main").load("pag_turma.php");
                    },
        error:      function(result){//if not successful run from here
                        }
    });    
}

function cad_professor(){
    console.log("enviado para BD");
    var nome = $("#inputNomeProf").val();
    var snome = $("#inputSnomeProf").val();
    var cpf = $("#inputCPFProf").val();
    var rg = $("#inputRGProf").val();
    var cidade = $("#inputCidadeProf").val();
    var rua = $("#inputRuaProf").val();
    var cep = $("#inputCepProf").val();
    var email = $("#inputEmailProf").val();
    var tel = $("#inputTelefoneProf").val();
    var login = $("#inputLoginProf").val();
    var senha = $("#inputPasswordProf").val();

    $.ajax({
                type: 'POST',//type: POST/GET
                url: '../php/config.php',//page to where the data goes
                data: { "login" : login,
                        "senha" : senha,
                        "email" : email,
                        "tel" : tel,
                        "rua" : rua,
                        "cep" : cep,
                        "nome" : nome,
                        "snome" : snome,
                        "cpf" : cpf,
                        "rg" : rg,
                        "cid" : cidade,
                        "user_type" : 1,
                        "professor" : 1},//type of data to be sent
                cache: false,//don't save in cache
                //"result" variable stores the data sent from server to client
                success:    function(result){//if successful run from here
                                console.log(result);
                            },
                error:      function(result){//if not successful run from here
                            }
                });
}

//função que será chamada no cadastro de alunos
function cad_aluno(){
    console.log("enviado para BD");
    var sexo = $('input[name="sex"]:checked').val();
    var data_nasc = $("#inputDatanascAluno").val();
    var nome = $("#inputNomeAluno").val();
    var snome = $("#inputSnomeAluno").val();
    var cidade = $("#inputCidadeAluno").val();
    var rua = $("#inputRuaAluno").val();
    var cep = $("#inputCepAluno").val();
    var email = $("#inputEmailAluno").val();
    var login = $("#inputLoginAluno").val();
    var senha = $("#inputPasswordAluno").val();

    $.ajax({
                type: 'POST',//type: POST/GET
                url: '../php/config.php',//page to where the data goes
                data: { "login" : login,
                        "senha" : senha,
                        "email" : email,
                        "rua" : rua,
                        "cep" : cep,
                        "nome" : nome,
                        "snome" : snome,
                        "cid" : cidade,
                        "data_nasc" : data_nasc,
                        "sexo" : sexo,
                        "user_type" : 2,
                        "aluno" : 1},//type of data to be sent
                cache: false,//don't save in cache
                //"result" variable stores the data sent from server to client
                success:    function(result){//if successful run from here
                                console.log(result);
                            },
                error:      function(result){//if not successful run from here
                            }
                });
}

//função que será chamada no cadastro de disciplinas
function cad_disc(){
    console.log("enviado para BD");
    var nome = $('#inputNome').val();
    var carga = $("#inputCarga").val();
    $.ajax({
                type: 'POST',//type: POST/GET
                url: '../php/config.php',//page to where the data goes
                data: { "nome" : nome,
                        "carga_h" : carga,
                        "disciplina" : 1},//type of data to be sent
                cache: false,//don't save in cache
                //"result" variable stores the data sent from server to client
                success:    function(result){//if successful run from here
                                console.log(result);
                            },
                error:      function(result){//if not successful run from here
                            }
                });
}

//função que será chamada no cadastro de turmas
function cad_turma(){
    //pegar o id da disciplina selecionada
    var disc_id = $("#disc_selected").children(":selected").attr("id");
    //pegar o id do professor selecionado
    var prof_id = $("#prof_selected").children(":selected").attr("id");
    var horario = $("#inputHor").val();
    var local = $("#inputLocal").val();
    //pegar os id's de todos os alunos selecionados
    var alunos_id = [];
    $("input:checkbox").each(function(){
        var $this = $(this);
        if($this.is(":checked")){
            alunos_id.push($this.attr("id"));
        }
    });
    $.ajax({
                type: 'POST',//type: POST/GET
                url: '../php/config.php',//page to where the data goes
                data: { "disc_id" : disc_id,
                        "prof_id" : prof_id,
                        "horario" : horario,
                        "local" : local,
                        data: { alunos_array : alunos_id},
                        "turma" : 1},//type of data to be sent
                cache: false,//don't save in cache
                //"result" variable stores the data sent from server to client
                success:    function(result){//if successful run from here
                                console.log(result);
                            },
                error:      function(result){//if not successful run from here
                            }
    });
}